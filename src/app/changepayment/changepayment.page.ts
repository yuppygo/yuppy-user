


import { Component, OnInit } from '@angular/core';
import { IoncabServicesService } from '../ioncab-services.service';
import {  Router } from '@angular/router';

@Component({
  selector: 'app-changepayment',
  templateUrl: './changepayment.page.html',
  styleUrls: ['./changepayment.page.scss'],
})
export class ChangepaymentPage implements OnInit {
  totalFare: number;
  
  constructor(public serviceProvider:IoncabServicesService,public route:Router) { }

  ngOnInit() {
    this.totalFare = Math.round(this.serviceProvider.tripDistance * this.serviceProvider.farePerKm)

  }

  submit(){
    this.route.navigate(['requestride'])

  }

}
