

import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChangepaymentPage } from './changepayment.page';

describe('ChangepaymentPage', () => {
  let component: ChangepaymentPage;
  let fixture: ComponentFixture<ChangepaymentPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChangepaymentPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChangepaymentPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
