

import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FareestimatePage } from './fareestimate.page';

describe('FareestimatePage', () => {
  let component: FareestimatePage;
  let fixture: ComponentFixture<FareestimatePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FareestimatePage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FareestimatePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
