

import { Component, OnInit } from '@angular/core';
import { IoncabServicesService } from '../ioncab-services.service';

@Component({
  selector: 'app-fareestimate',
  templateUrl: './fareestimate.page.html',
  styleUrls: ['./fareestimate.page.scss'],
})
export class FareestimatePage implements OnInit {
  totalFare: number;

  constructor(public serviceProvider: IoncabServicesService) { }

  ngOnInit() {
    this.totalFare = Math.round(this.serviceProvider.tripDistance * this.serviceProvider.farePerKm)

  }

}
