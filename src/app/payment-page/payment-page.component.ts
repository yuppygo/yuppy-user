

import { Component, OnInit } from '@angular/core';
import { IoncabServicesService } from '../ioncab-services.service';
import { ModalController } from '@ionic/angular';
import { Router } from '@angular/router';
import { ViewController } from '@ionic/core';
@Component({
  selector: 'app-payment-page',
  templateUrl: './payment-page.component.html',
  styleUrls: ['./payment-page.component.scss']
})
export class PaymentPageComponent implements OnInit {
  destination: any;
  carName: any;
  totalFare: number;
  valueSelected = 1;
  constructor(public serviceProvider: IoncabServicesService, public modalCtrl: ModalController, public route: Router) {
    this.totalFare = Math.round(this.serviceProvider.tripDistance * this.serviceProvider.farePerKm)
  }
  closeModal() {
    this.modalCtrl.dismiss();
  }
  async routeModal() {
    console.log(this.valueSelected);
    if (this.serviceProvider.showdestination === '') {
      this.modalCtrl.dismiss();
      const toast: any = await this.serviceProvider.presentToast('You Must select Destination Location First For Estimate Fare');
      await toast.present();
    } else {
      this.modalCtrl.dismiss();
      this.route.navigate(['requestride'], {queryParams: { payment: this.valueSelected }});
    }
  }

  ngOnInit() {
  }
}
